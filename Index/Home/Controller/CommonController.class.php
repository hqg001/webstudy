<?php

namespace Home\Controller;

use Think\Controller;

class CommonController extends Controller{
	public function _initialize(){
		if(!isset($_COOKIE['auto'])){
			$value=explode("|",encryption($_COOKIE['auto'],1));
			// 判断登陆ip是否和上一次登陆相同
			$ip=get_client_ip();
			if($ip=$value[1]){
				$account=$value[0];
				$where=array(
				'account'=>$account
				);
				$user=M('user')->$where($where)->field(array(
				'id',
				'lock'
				))->find();
			}
			// 判断用户是否被锁定
			if($user&&!$user['lock']){
				session('uid',$user['id']);
			}
		}
		
		// session不存在，返回登陆页面
		if(!isset($_SESSION['uid'])){
			redirect(U("Login/index"));
		}
	}
}
?>