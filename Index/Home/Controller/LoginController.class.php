<?php

namespace Home\Controller;

use Think\Controller;

class LoginController extends Controller
{
    public function index()
    {
        $this->display();
    }

    public function login()
    {
        if (!IS_POST) {
            $this->error("页面不存在");
        }
        $account = I("account");
        $pwd = md5(I("pwd"));
        $auto = I("auto");
        // echo $account.$pwd." auto:".$auto;

        $where = array(
            'account' => $account
        );
        $user = M('User')->where($where)->find();
        // dump($user);

        // 用户不存在或密码错误
        if (!$user || $user ['password'] != $pwd) {
            $this->error('用户不存在或密码错误');
        }

        // 判断用户是否被锁定
        if ($user ['lock']) {
            $this->error('用户被锁定');
        }

        // 判断是否勾选自动登陆
        if (isset ($_POST ['auto'])) {
            $account = $user ['account'];
            $ip = get_client_ip();
            $value = $account . '|' . $ip;
            $value = encryption($value, 0);
            @setcookie('auto', $value, C('AUTO_LOGIN_TIME'), '/');
        }

        session('uid', $user ['id']);
        header('Content-Type:text/html;Charset=UTF-8');
        redirect(__APP__, 3, "登陆成功，请稍后。。。。。。");

    }

    /**
     * 注册页面
     */
    public function register()
    {
        if (!C('REGIS_ON')) {
            $this->error("暂停注册", U('index'));
        }
        $this->display();
    }

    /**
     * 表单注册处理
     */
    public function runRegis()
    {
        if (!IS_POST) {
            $this->error("页面不存在");
        }
        $account = I('account');
        $pwd = I('pwd');
        $pwded = I('pwded');
        $uname = I('uname');
        $verify = I('verify');
        if ($pwd != $pwded) {
            $this->error("密码不一致");
        }

        //提交POST数据
        $data = array(
            'account' => $account,
            'password' => md5($pwd),
            'registime' => time(),
            'userinfo' => array(
                'username' => $uname,
            ),
        );
        $id = D('User')->insert($data);
        if ($id) {
            //插入数据成功后把用户ID写SESSION
            session('uid', $id);
            header('Content-Type:text/html;Charset=UTF-8');
            redirect(__APP__, 3, '注册成功，页面跳转中...');
        } else {
            $this->error('注册失败，请重试...');
        }
    }


    /**
     *验证码
     */
    public function verify()
    {
        ob_clean();
        $config = array(
            'fontSize' => 400,    // 验证码字体大小
            'length' => 4,     // 验证码位数
            'useNoise' => true, // 关闭验证码杂点
            'reset' => false // 验证成功后是否重置，————这里是无效的。
        );
        $Verify = new \Think\Verify($config);
        $Verify->entry();
    }

    /*
    * 异步账号是否已存在
   */

    public function checkAccount(){
        if(!IS_AJAX){
            $this->error('页面不存在');
        }
        $account = I('account');
        $where = array('account'=>$account);
        if(M('user')->where($where)->getField('id')){
            echo 'false';
        }else{
            echo 'true';
        }
    }

    /*
     * 异步验证昵称是否已存在
     */
    public function checkUname(){
        if(!IS_AJAX){
            $this->error('页面不存在');
        }
        $account = I('username');
        $where = array('username'=>$account);
        if(M('user')->where($where)->getField('id')){
            echo 'true';
        }else{
            echo 'false';
        }

    }

    /*
     * 异步验证验证码
     */

    public function checkVerify(){
        $code=I('verify');
        if(check_verify($code)){
            echo 'true';
        }else {
            echo 'flase';
            //echo 'false';//测试关闭
        }

    }
}

?>