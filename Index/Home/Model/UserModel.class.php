<?php

namespace Home\Model;

use Think\Model\RelationModel;

class UserModel extends RelationModel {
	//定义主表名称
	protected $tableName = 'user';
	
	//定义用户与用户信息处理表关系属性
	Protected $_link = array(
		'userinfo' =>array(
			'mapping_type'=>self::HAS_ONE,
			'foreign_key'=> 'uid',
		)	
	);
}
?>