<?php
/*
 * 格式化打印数据
 */
function p($arr) {
	echo '<pre>';
	print_r ( $arr );
	echo '</pre>';
	die ();
}
function p2($arr) {
	echo '<pre>';
	print_r ( $arr );
	echo '</pre>';
}
function check_verify($code, $id = '') {
	$config = array (
			'reset' => false  // 验证成功后是否重置，—————这里才是有效的。
		);
	$verify = new \Think\Verify ( $config );
	return $verify->check ( $code, $id );
}

/*
 * 异位或加密字符串 @param [String]	
 * $value	[需要加密的字符串 @param [integer]	
 * $type	[加密解密(0:加密，1：解密)]
 *  @return[String]	[加密或解密后的字符串]
 */
function encryption($value, $type = 0) {
	$key = md5 ( C ( 'ENCTYPTION_KEY' ) );
	if (! $type) {
		return str_replace ( '=', '', base64_encode ( $value ^ $key ) );
	}
	$value = base64_decode ( $value );
	return $value ^ $key;
}

?>