<?php
return array(
    //数据库配置
    'DB_TYPE' => 'mysql', // 数据库类型
    'DB_HOST' => 'localhost', // 服务器地址
    'DB_NAME' => 'weibo',          // 数据库名
    'DB_USER' => 'root',      // 用户名
    'DB_PWD' => '',          // 密码
    'DB_PREFIX' => 'hd_',    // 数据库表前缀
    'DEFAULT_THEME' => 'default',
    'SHOW_PAGE_TRACE' => true,

    //自动登陆保存时间
    'AUTO_LOGIN_TIME' => time() + 3600 * 24 * 7, //一个星期

    //用于异位或加密的KEY
    'ENCTYPTION_KEY' => 'www.test.com',

    //加载扩展配置文件
    'LOAD_EXT_CONFIG' => 'system',

//    /* 错误页面模板 */
//    // 默认错误跳转对应的模板文件'
//    'TMPL_ACTION_ERROR' => 'Common/error.html',
//    // 默认成功跳转对应的模板文件'
//    'TMPL_ACTION_SUCCESS' => 'Common/success.html',
//    // 异常页面的模板文件
//    'TMPL_EXCEPTION_FILE' => 'Common/exception.html',


);